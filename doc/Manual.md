
![SlimFTPd64 Logo](./images/SlimFTPd64.png)

# SlimFTPd64 | Program Operation Guide

 <div class="page-break"></div>

 ## Quick Start

 ### Topics

 This document will cover the following :

 * **Overview**
 * **Recommended Specs**
 * **Downloading SlimFTPd64**
 * **Installation**
 * **Running SlimFTPd64 for the first time**
 * **Usage tips**

<div class="page-break"></div>

###  Overview

**SlimFTPd64** is a light weight ftp server application which runs on Windows 10.

This document will cover running the application for the first time.

### Recommended Specs

In most cases, a modern desktop or laptop computer which has Windows 10 (64 bit edition) installed can run SlimFTPd64.

### Downloading SlimFTPd64

SlimFTPd64 is available as a binary release package; including supporting documents such as this guide. The source code is available via [Gitlab][deb4dd63].

  [deb4dd63]: https://gitlab.com/TW3/slimftpd64 "SlimFTPd64 Gitlab project page"

Binary packages are built completely transparently by the good folks at [Appveyor][63c77fef]. They can be downloaded from the [SlimFTPd64 Homepage][30e2d50a]

  [30e2d50a]: https://tw3.gitlab.io/slimftpd64/ "The SlimFTPd64 Website Homepage"

A sha256 checksum of the release archive is posted onto the Homepage and can be viewed directly below the download button. The zip archive's checksum can be verified by using the code:

`certUtil -hashfile SlimFTPd64-latest-win10-x86_64.zip SHA256`

Via a command prompt instance.
The sha256 checksum is also shown within the [Appveyor][63c77fef] [build log][20944149] where it is generated.

[63c77fef]: http://appveyor.com/ "Appveyor"
  [20944149]: https://ci.appveyor.com/project/TonyW/SlimFTPd64 "SlimFTPd64 build log"

_You can alternatively compile the source code using Microsoft Visual Studio 2019 (or newer,) instead if you prefer._

### Installation

Once you have obtained a copy of the SlimFTPd64 archive and extracted it or compiled the software, copy the **SlimFTPd64** folder containing the file **SlimFTPd64.exe**, to any hard disk location.
A recommended location to do so is `C:\Programs\SlimFTPd64\` but you can save the **SlimFTPd64** folder absolutely anywhere you like.

<div class="page-break"></div>

### Running SlimFTPd64 for the first time

Once the SlimFTPd64 folder has been copied to it's location, navigate inside the folder and double click on the file **SlimFTPd64.exe**. All things being equal, the application should start.

![Windows Firewall Application Window](./images/firewall.png)

The very first time the application is run, a request to allow access through the Windows firewall will happen. Grant the application access to the networks you want the ftp server to run on. For reference in case your needs include opening a firewall port; the default port number is **21**.

When the server has successfully started, the application's tray icon will appear in the notification area of the taskbar.

![Windows System Tray](./images/tray.png)

<div class="page-break"></div>

If the tray icon cannot be seen immediately, then it can be made visible by opening the Windows Settings application and visiting: _**System** > **Personalisation** > **Taskbar** > **Select which icons appear in the taskbar**_ and setting **slimftpd64.exe** to **On**. The Windows Settings application can be started by right clicking on the Windows Start Menu and selecting **Settings**.

**To stop SlimFTPd64**:- Right click on the tray icon and select "**Shutdown SlimFTPd64**." The application will close and the ftp server will shutdown and quit.

**That's all there is to starting SlimFTPd64 for the first time!**

<div class="page-break"></div>

### Usage tips

The file **SlimFTPd64.conf** provides the ability to configure the ftp server. It is recommended that the server is restarted after any changes to the configuration are made. The file is well commented and fairly self explanatory. For many, the default configuration will suffice. The default does _not_ allow write access and permits read access without a password to the user account **anon**. Please feel free to change the settings to meet your own needs and requirements.

The folder **ftproot** is provided for convenience and is the default ftp folder for the user **anon**. Please feel free to add your files into the folder or even remove it and select another location instead.

If the server fails to start or for any other diagnostic purposes you want to observe the inner workings of the application, please view the log file **SlimFTPd64.log** located inside of the application directory.

Start SlimFTPd64 using the `-q` command line switch to disable the tray icon. You will need to manually end the application from the task manager if you decide to use this method and want to restart or stop the ftp server.

A simple test to establish working ftp service might be done using a Linux machine running busybox (which has the ftpget application compiled in.) To retrieve files being served via SlimFTPd64; from the Linux machine try something like:

`ftpget -u anon 192.168.0.5 test.txt test.txt`

(_replacing **192.168.0.5** with the ip address of the machine running SlimFTPd64_) To retrieve a file named **test.txt** being served by the ftp server.

#### Appendix:

>  *Be careful when transferring text files from Windows to Linux machines. By default, files created using Windows text editors have different line endings to those created using Linux. Create files using Unix line endings on Windows to be able to read them easily after they have been transferred across to a machine running Linux.*
