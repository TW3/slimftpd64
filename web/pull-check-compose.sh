#!/usr/bin/env sh

cd web/data/

if [ ! -f composer.phar ]; then

	php -r "copy('https://getcomposer.org/download/1.8.5/composer.phar', 'composer.phar');"
	ACTUAL_SIGNATURE="$(php -r "echo hash_file('sha256', 'composer.phar');")"
	EXPECTED_SIGNATURE="4e4c1cd74b54a26618699f3190e6f5fc63bb308b13fa660f71f2a2df047c0e17"

	if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
		then
		>&2 echo 'ERROR: Invalid installer signature'
		exit 1
	fi

    apk update
    apk add libzip-dev
    docker-php-ext-install zip

	if [ -d "$CI_PROJECT_NAME" ]; then
		mv "$CI_PROJECT_NAME/" katana-old
	fi

	php composer.phar create-project themsaid/katana $CI_PROJECT_NAME
	mv $CI_PROJECT_NAME/config.php ../config.old
	mv $CI_PROJECT_NAME/content/ ../content-old/

fi

if [ ! -d "$CI_PROJECT_NAME" ]; then
	mkdir "$CI_PROJECT_NAME"
fi

mv "content/" "$CI_PROJECT_NAME/content/"
mv "config.php" "$CI_PROJECT_NAME/config.php"
sed -i -- 's#'"placeholder"'#'"$CI_PROJECT_NAME"'#g' "$CI_PROJECT_NAME/config.php"
