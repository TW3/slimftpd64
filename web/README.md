
# SlimFTPd64 - Web code

Website code for https://gitlab.com/TW3/slimftpd64
The site can be found at https://tw3.gitlab.io/slimftpd64

Please do not make changes to the contents found inside
this folder and commit them via git unless you are a
project administrator. Lest they be reverted.

Thanks!
