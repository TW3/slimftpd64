<section>
    <h3>Download</h3>
    <aside>
        <span>Grab the latest version here:</span>
        <br /><br />

        <div class="popover popover-bottom">
            <form style="display: inline" action="@url('assets/dl/'){{ $appArchive }}" method="get"><button class="btn btn-success">Download SlimFTPd64 <i class="icon icon-link"></i></button></form>
            <div class="popover-container">
                <div class="card bg-dark">
                    <div class="card-header">
                        <h3 class="text-light">Warning:</h3>
                    </div>
                    <div class="card-body"><span class="text-warning bg-dark">SlimFTPd64 runs on 64 bit Microsoft Windows® 10 only.</span><br /><br /><span class="text-gray">Please <a href="https://gitlab.com/TW3/slimftpd64/issues" class="external-link">report</a> any bugs you find. <b>Thank You!</b></span></div>
                    <div class="card-footer"><i class="icon icon-download text-gray"></i></div>
                </div>
            </div>
        </div>

        <br /><br />
        <span>SlimFTPd64 is licensed under the terms of the <a href="https://gitlab.com/TW3/slimftpd64/blob/staging/LICENSE.txt" class="external-link">BSD License</a>.</span>
        <br />
        <span>The sha256 checksum for the file {{ $appArchive }} is:</span>
        <br /><br />
        <div class="bg-dark" id="check-sum"><div class="text-bold"><span class="text-gray"><small>{{ $appChecksum }}</small></span></div></div>
        <br /><hr /><br />

        <div class="container">
            <div class="columns">
                <div class="column col-3">
		            <div class="card" id="card-support-av">
			            <div class="card-header">
				            <div class="card-title h5">Built by</div>
			            </div>
			            <div class="card-image col-mx-auto"><a href="https://ci.appveyor.com/project/TonyW/SlimFTPd64" class="external-link"><img src="https://ci.appveyor.com/api/projects/status/suxq0290i26rygl5?retina=true" class="img-responsive width-128" alt="Appveyor Build Status" /></a></div>
			            <div class="card-footer">Appveyor</div>
		            </div>
	            </div>
            </div>
        </div>

        <br />
        <span class="text-dark"><a href="https://www.appveyor.com/" class="external-link">Appveyor</a> provide continuous integration tools for Windows® developers.</span>
        <br />
    </aside>
</section>