
#define PACKAGE "SlimFTPd64"
#define PACKAGE_BUGREPORT "https://gitlab.com/TW3/SlimFTPd64/issues"
#define PACKAGE_NAME "SlimFTPd64"
#define PACKAGE_STRING "SlimFTPd64 0.x"
#define PACKAGE_TARNAME "SlimFTPd64"
#define PACKAGE_URL "https://gitlab.com/TW3/SlimFTPd64/releases"
#define PACKAGE_VERSION "0.x"
#define VERSION "0.x"
