![SlimFTPd64 Logo](./doc/images/SlimFTPd64.png)

# SlimFTPd64

**NEW! AND IMPROVED!**

## SlimFTPd64 is a fork of SlimFTPd the fully standards-compliant FTP server implementation with an advanced virtual file system. It is extremely small, but don't let its file size deceive you: SlimFTPd64 packs a lot of bang for the kilobyte. It is written in C++ and requires no messy installer.

**SlimFTPd64** runs quietly in the background. It reads its configuration from a configuration file in the same folder as the executable, and it outputs all activity to a log file in the same place. The virtual file system allows you to mount any local drive or path to any virtual path on the server. This allows you to have multiple local drives represented on the server's virtual file system or just different folders from the same drive. SlimFTPd64 allows you to set individual permissions for server paths. Open slimftpd64.conf using your favourite text editor to set up SlimFTPd64's configuration. The format of SlimFTPd64's config file is similar to that of the Apache Web Server.
<br></br>

### SlimFTPd64 features:

* _Standards-compliant FTP server implementation that works with all major FTP clients_
* _Fully multi-threaded 64-bit Windows 10 application_
* _Supports passive mode transfers and allows resume of failed transfers_
* _Small memory footprint; won't hog system resources_
* _Easy configuration of server options through configuration file_
* _All activity logged to file_
* _Support for binding to a specific interface in multihomed environments_
* _User definable timeouts_
* _No installation routine; won't take over your system_
* _Supports all standard FTP commands: ABOR, APPE, CDUP/XCUP, CWD/XCWD, DELE, HELP, LIST, MKD/XMKD, NOOP, PASS, PASV, PORT, PWD/XPWD, QUIT, REIN, RETR, RMD/XRMD, RNFR/RNTO, STAT, STOR, SYST, TYPE, USER_
* _Supports these extended FTP commands: MDTM, NLST, REST, SIZE_
* _Supports setting of file timestamps_
* _Conforms to [RFC 959](http://www.ietf.org/rfc/rfc0959.txt) and [RFC 1123](http://www.ietf.org/rfc/rfc1123.txt) standards_

### Changes from original SlimFTPd 3.181

* _Added a tray icon_
* _64bit Windows executable_
* _Dropped support for non-NT OSes_
* _Unicode support_
* _Minor bug fixes_
